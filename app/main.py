from typing import Union
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from datetime import datetime
import os
import sqlite3

app = FastAPI()

@app.get("/")
def read_root():
    return {"Code+": "Campus Space & Energy Management System (The best group!)"}

@app.get("/uptime")
def return_uptime():
    awake_since = os.popen('uptime -p').read()
    right_now = datetime.now()
    return {"server_uptime": awake_since, "current_timestamp": right_now}

class CO2Reading(BaseModel):
    timestamp: datetime
    room: str
    co2: int

# Use o caminho absoluto para o banco de dados
database_path = "/app/co2_readings.db"

# Conectar ao banco de dados SQLite (ou criar se não existir)
conn = sqlite3.connect(database_path)
cursor = conn.cursor()

# Criar a tabela se não existir
cursor.execute('''
CREATE TABLE IF NOT EXISTS co2_readings (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    timestamp TEXT,
    room TEXT,
    co2 INTEGER
)
''')
conn.commit()

@app.post("/co2_readings/")
async def create_co2_reading(reading: CO2Reading):
    try:
        # Inserir os dados na tabela
        cursor.execute('''
        INSERT INTO co2_readings (timestamp, room, co2)
        VALUES (?, ?, ?)
        ''', (reading.timestamp.strftime('%Y-%m-%d %H:%M:%S'), reading.room, reading.co2))
        
        conn.commit()
        
        return {"message": "CO2 reading added successfully.", "data": reading}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
