FROM python:3.9

LABEL maintainer="Mark McCahill <mccahill@duke.edu>"
ARG NB_USER="jovyan"
ARG NB_UID="1000"
ARG NB_GID="1000"
ARG APP_DIR="/app"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

USER root

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
    apt-get upgrade --yes && \
    apt-get install --yes --no-install-recommends \
    wget \
    curl \
    locales \
    net-tools \
    build-essential \
    git \
    bzip2 \
    vim \
    unzip \
    ssh \
    htop \
    jq \
    libcurl4 && \
    apt-get clean && rm -rf /var/lib/apt/lists/* 

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

ENV SHELL=/bin/bash \
    NB_USER="${NB_USER}" \
    NB_UID=${NB_UID} \
    NB_GID=${NB_GID} \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    HOME="/home/${NB_USER}" \
    PATH="/home/${NB_USER}/.local/bin:${PATH}" 

COPY fix-permissions /usr/local/bin/fix-permissions
RUN chmod a+rx /usr/local/bin/fix-permissions

RUN sed -i 's/^#force_color_prompt=yes/force_color_prompt=yes/' /etc/skel/.bashrc 

RUN groupadd --gid $NB_GID jovyan && \
    useradd -l -m -s /bin/bash -N -u $NB_UID --gid $NB_GID $NB_USER && \
    chmod g+w /etc/passwd && \
    fix-permissions "${HOME}" 

RUN pip install --upgrade pip

COPY requirements.txt /home/jovyan/
RUN pip install -r /home/jovyan/requirements.txt

COPY ./app $APP_DIR/

RUN chmod -R a+rw $APP_DIR

WORKDIR $APP_DIR

USER $NB_USER

EXPOSE 8080
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8080"]
