# rest-api-demo-docker
Super simple FastAPI REST API framework example in a Docker container

## Buiding and running via Docker

The build script will create a docker image name 'fastapi'. 
After you have built the docker image you can run it with the script named 'run'.

## To do

Plenty of things to do here before it would be production ready. 
For instance:
- update the REST API endpoints to do something more interestinbg
- the REST API will probably need an Nginx proxy to provide https 
- add HTTP Basic Auth to limit access to the API to autorized entities


